# mixertui 1.5

Audio Mixer with a Text User Interface for [FreeBSD](https://www.freebsd.org).

## Getting Started

To install the port [audio/mixertui](https://www.freshports.org/audio/mixertui):

	# cd /usr/ports/audio/mixertui/ && make install clean

To add the package:

	# pkg install mixertui

To run after installation:

	% mixertui


![screenshot](screenshot.png)  

## Documentation

Usage:

```
% mixertui -h
usage: mixertui -h | -v | -P <profile>
       mixertui [-c] [-d <unit>] [-m <mixer>] [-p <profile>] [-V p|c|a]

 -c		 Disable color.
 -d <unit>	 Open device with <unit> number.
 -h		 Display this help and quit.
 -m <mixer>	 Use <mixer> to get devices.
 -P <profile>	 Set <profile> and quit (EXPERIMENTAL).
 -p <profile>	 Set <profile> (EXPERIMENTAL).
 -V p|c|a	 Display mode p[layback] | c[apture] | a[ll].
 -v		 Show version and quit.

Press F1 inside MixerTUI for runtime features.
See 'man mixertui' for more information.
```

Manual: 

 - `% man 8 mixertui`
 - [online](https://man.freebsd.org/cgi/man.cgi?query=mixertui&sektion=8&manpath=freebsd-ports)


## Building from sources

Step 1. Install deps:

	# pkg install bsddialog libsysctlmibinfo2

Step 2. Download, build, and run:

	% git clone https://gitlab.com/alfix/mixertui.git
	% cd mixertui
	% make
	% ./mixertui
